<script type="text/javascript">/* [%TICKET_NUM%] - V.1.1 */
// SET SKIN ATTRIBUTES
    var qaCreativeURL = "[%IMAGE_URL%]"; 
    var qaWidth = [%WIDTH%]; 
    var qaHeight = [%HEIGHT%]; 
    var qaClickThru = "[%CLICKTHROUGH_URL%]"; 
    var qa3PImpressions = ["[%IMP_PIXEL1%]","[%IMP_PIXEL2%]","[%IMP_PIXEL3%]","[%IMP_PIXEL4%]","[%IMP_PIXEL5%]"];   
    var qaSkinPos = "[%POSITION%]"; 
    var qaClickable = [%CLICKABLE%];  
    var qaTopPos = [%TOP%]; 
    var qaBackgroundColor = "[%BACKGROUND_COLOR%]";  

// CLICK TRACKING
    var qaClickTracker = "%%CLICK_URL_UNESC%%";
    var qaImpTracker = "%%VIEW_URL_UNESC%%";
    
    var qaClickPixel = "http://i.cdn.turner.com/ttn/ttn_adspaces/1.0/creatives/2012/2/10/1529111x1.gif";

// POST SKIN IMAGE TO PAGE
    var bodyTag = parent.document.getElementsByTagName('body')[0];
          bodyTag.style.overflowX = "hidden";

    bodyTag.style.background = qaBackgroundColor+" url('"+qaCreativeURL+"') no-repeat "+qaSkinPos+" center "+qaTopPos+"px"; 
 
// BUILD IMPRESSION AND CLICK TRACKING ELEMENTS
    var impImg = document.createElement('img');
    var trkPixImg = document.createElement('img');
    var leftWing = document.createElement('div');
    var rightWing = document.createElement('div');

//SET THE CLICKABLE AREA FOR THE SKIN
	if(qaClickable){
		rightWing.setAttribute('id', 'rightWing');
		rightWing.style.height = qaHeight+"px";
		rightWing.style.width = "25%";
		rightWing.style.right = "25%";
		rightWing.style.marginRight = "-505px";
		rightWing.style.position = (qaSkinPos=="scroll" ? "absolute":"fixed");
		rightWing.style.zIndex = "1";
		rightWing.innerHTML = "<a href="+qaClickTracker+qaClickThru+" target='_blank'><img src="+qaClickPixel+" width=1 height="+qaHeight+" border=0 style='width:99%; position:relative; z-index:2;'></a>";
		bodyTag.insertBefore(rightWing, bodyTag.firstChild);
		
		leftWing.setAttribute('id', 'leftWing');
		leftWing.style.height = qaHeight+"px";
		leftWing.style.width = "25%";
		leftWing.style.left = "25%";
		leftWing.style.marginLeft = "-505px";
		leftWing.style.position = (qaSkinPos=="scroll" ? "absolute":"fixed");
		leftWing.style.zIndex = "1";
		leftWing.innerHTML = "<a href="+qaClickTracker+qaClickThru+" target='_blank'><img src="+qaClickPixel+" width=1 height="+qaHeight+" border=0 style='width:99%; position:relative; z-index:2;'></a>";
		bodyTag.insertBefore(leftWing, rightWing);
	}

//BUILD THE IMPRESSION TRACKING TAG
	impImg.setAttribute('src',  qaImpTracker+qaClickPixel);

//BUILD THE 3RD PARTY IMPRESSION PIXELS
    for (var index = 0; index < qa3PImpressions.length; index++){
        trkPixImg = document.createElement('img');
        trkPixImg.setAttribute('src', qa3PImpressions[index]);
        trkPixImg.setAttribute('width', "1");
        trkPixImg.setAttribute('height', "1");
        trkPixImg.style.position = 'absolute';
        trkPixImg.style.visibility = 'hidden';
        leftWing.appendChild(trkPixImg);
    }    
    leftWing.appendChild(impImg);
    
    
    var dfpSkinDiv = parent.document.getElementById("ad_oop_skin_01");
    if(dfpSkinDiv){dfpSkinDiv.style.display = "none";}

</script>
